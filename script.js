function createNewUser() {

    const newUser = {

        _firstName: prompt('Enter First Name'),
        _lastName: prompt('Enter Last Name'),

        getLogin() {
            return this._firstName[0].toLowerCase() + this._lastName.toLowerCase();
        }
    };

    Object.defineProperty(newUser, "firstName", {
        set: function setFirstName(value) {
            this._firstName = value;
        },
        get: function setFirstName() {
            return this._firstName;
        },
    });

    Object.defineProperty(newUser, "lastName", {
        set: function setLastName(value) {
            this._lastName = value;
        },
        get: function setLastName() {
            return this._lastName;
        },
    });

    return newUser;
    
}

let newUser = createNewUser();

console.log(newUser);
console.log(newUser.getLogin());


